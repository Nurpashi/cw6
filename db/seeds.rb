# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = []

10.times do
  name = Faker::Name.name
  users << User.create(name: name,
           email: Faker::Internet.email(name),
           password: '12345678',
           password_confirmation: '12345678')
end

comments = []

count = 0
40.times do
  count += 1
  comments_array = []

  comments_array << count
  grade = Faker::Number.between(1, 5)
  comments << Comment.create(grade: grade,
              comment: Faker::Lorem.sentence,
              user: users.sample)

end


20.times do
  comments_array = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,
  27,28,29,30,31,32,33,34,35,36,37,38,39,40]
  Image.create(title: Faker::Lorem.sentence,
               image: Faker::Company.logo, # ���� ����� ��� ��������
               user: users.sample,
               comment_id: comments_array.sample)
end
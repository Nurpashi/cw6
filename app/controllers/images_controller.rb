class ImagesController < ApplicationController
  def index
    @images = Image.order(created_at: :desc)
  end

  def show
    @image = Image.find(params[:id])
  end

  def new
    @image = Image.new
  end

  def create
    @image = current_user.image.built(image_params)
    # ���� ����� ����� ������������� ��������� ����� � ���� �����
    If @image.save
    flash[:success] = "Your image has been created successfully"
    redirect_to root_url
  else
    render 'new'
  end


  def edit
  end

  private
  def image_params
    params.require(:image).permit(:title, :image, :user_id)
  end

  def correct_user
    @image = Image.find(params[:id])
    unless @image.user == current_user # || current_user.admin?
      flash[:success] = 'Your image has been created succesfully'
      redirect_to root_url
    else
      render 'new'
    end
  end


end

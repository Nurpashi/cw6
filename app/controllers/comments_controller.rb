class CommentsController < ApplicationController
  def index
    @comments = Comment.all
  end

  def show
    @user = User.find(params[:id])
    @comments = @user.comments.order(created_at: :desc)
  end


  def new
  end

  def edit
  end
end
